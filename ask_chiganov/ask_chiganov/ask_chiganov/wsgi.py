"""
WSGI config for ask_chiganov project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os, sys
sys.path.append('/var/www/ask_chiganov/ask_chiganov/')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ask_chiganov.settings")


from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
