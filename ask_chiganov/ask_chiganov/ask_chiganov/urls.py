from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ask_chiganov.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^idx', 'helloworld.views.idx'),
    url(r'^wsgi/', 'helloworld.views.wsgi'),
    
    url(r'^signup/', 'helloworld.views.signup'),
    url(r'^login', 'helloworld.views.login'),
    url(r'^$', 'helloworld.views.main'),
)
