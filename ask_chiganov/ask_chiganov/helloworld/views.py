from django.shortcuts import render
from django.http import HttpResponse
from django.http import QueryDict
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt

from django.shortcuts import redirect

default_dict = {"tags_size": ["12", "16", "20", "22", "24"],
                "tags": ["Meth", "Cook", "Money", "Blue", "MGTU", "MSK", "yar", "lol"],
                "users": ["Pinkman", "Hank", "Goodman", "Mike", "'Gus' Fring"],
                "rnd_col": ["red", "blue", "green", "black", "darkred", "orange", "darkgreen", "pink", "magenta", "lime"]}


@csrf_exempt
def wsgi(request):
    output = []
    if request.method == 'GET':
        d = request.GET
        output.append("GET <br>")
    else:
        d = request.POST
        output.append("POST <br>")
    for key,value in d.items():
        output.append("{0}  =  {1} ".format(key, value))
	output.append("<br>")
    return HttpResponse(output)


def idx(request):
    return HttpResponse("<h1>Hello World</h1>")

def main(request):
    return render(request, "index.html",default_dict)

def signup(request):
    d = default_dict.copy()
    if request.method == 'POST':
        tmp_dict = request.POST
        has_error = False
        if tmp_dict['name'] != 'Heisenberg':
            has_error = True
            d.update({'name_error': "true", 'logout': 'true'})
        if tmp_dict['password'] != '1234':
            has_error = True
            d.update({'password_error': 'true', 'loguot': 'true'})
        if has_error:
            return render(request, "signup.html", d)
        else:
            return redirect('ask.views.index')
    else:
        d.update(request.GET)
        return render(request, 'signup.html', d)

def login(request):
    d = default_dict.copy()
    if request.method == 'POST':
        tmp_dict = request.POST
        has_error = False
        if tmp_dict['name'] != 'Heisenberg':
            has_error = True
            d.update({'name_error': "true", 'logout': 'true'})
        if tmp_dict['password'] != '1234':
            has_error = True
            d.update({'password_error': 'true', 'logout': 'true'})
        if has_error:
            return render(request, "login.html", d)
        else:
            return redirect('ask.views.index')
    else:
        if 'logout' in request.GET:
            d['logout'] = True
        return render(request, "login.html", d)
