#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define uint unsigned int
#define MAX_NEW_SIZE_FIGURE 5 //двоичный корень из 32 
#define SIZE_INPUT_BUFF 8

char* enter()
{
    char **S = NULL;
    char **new_s = NULL;
    S = (char**) malloc(sizeof(char*));
    uint count = 0;
    uint j = 0;
    do {      
        new_s = (char**)realloc(S, sizeof(char*)*(count+1));
        if (new_s != NULL)
        {
            S = new_s;
        }
        else
        {
            free (S);
            S=NULL;
            return NULL;
        }

        S[count] = (char*) malloc((SIZE_INPUT_BUFF+1)*sizeof(char) );
        if (fgets(S[count],SIZE_INPUT_BUFF+1,stdin)!=NULL) 
        {
            j = 0;
            while(S[count][j]!='\0' && j<SIZE_INPUT_BUFF)
            {
                //printf("%c\n", S[count][j]);
                ++j;
            }
        }else
        {
            free(S[count]);
            free(S);
            return NULL;
        }
    } while(S[count++][j] != '\0');
    
    uint i = 0;
    char* out = (char*)malloc((count*SIZE_INPUT_BUFF)*sizeof(char)+1);
    for(i=0;i<count;++i)
    {
        memcpy(out+i*SIZE_INPUT_BUFF,S[i],SIZE_INPUT_BUFF);
        free(S[i]);
        S[i]=NULL;
    }
    free(S);
    S = NULL;
    out[(count*SIZE_INPUT_BUFF)*sizeof(char)]='\0';
    return out;
}

int num_translation(int num_p, int num_q, char *s, char** output)
{
    //проверка на корректность входных данных
    if((num_q<2)|
            (num_p>36)|
            (num_q>=num_p)|
            (s == NULL))
    {
        return 0;
    }
    //выяснение длины s и проверка на корректность и перевод во 10тичную СС
    int length_s = 0;
    unsigned long int num_s = 0;
    while ((s[length_s] != '\n')&
           (s[length_s] != '\0'))
    {
        int b_s = s[length_s];
        if(((b_s>='0')&(b_s<='9'))|
                ((b_s>='A')&(b_s<='Z'))|
                ((b_s>='a')&(b_s<='z'))) 	//между числами и буквами
        {									//есть символы, поэтому надо
            if(b_s>'Z')						//разделить диапазоны на 2 части
            {
                b_s=b_s-'a'+10;
            }
            else if(b_s>'9')
            {
                b_s=b_s-'A'+10;
            }
            else
            {
                b_s=b_s-'0';
            }
            if(b_s>=num_p) return 0; 		// ошибка цифра выходит за диапазон основания p
            num_s = num_s*num_p + (b_s);
            ++length_s;
            //printf("b_s=%d length_s=%d num_s=%lu\n",b_s,length_s,num_s);
        }
        else
        {
            return 0;
        }

    }

    char* out = (char*)malloc(MAX_NEW_SIZE_FIGURE*length_s*sizeof(char));
    unsigned long int b_s = num_s;
    int ii = 0;
    int module = num_q + 1;
    while(b_s!=0){
        module = b_s % num_q;
        char b_ch;
        b_s = b_s / num_q;
        if(module<=9)
        {
            b_ch = (char)module + '0';
        }else
        {
            b_ch = (char)module + 'A'-10;
        }
        out[ii]= b_ch;
        ++ii;
    //    printf("module=%d ii=%d b_ch=%c out=%s\n",module,ii,b_ch,out);
    }
    if (ii!=0)
    {
        out[ii] = '\0';
    }else
    {
        out[0] = '0';
        out[1] = '\0';
        *output = out;
        return 1;
    }

    int new_length_s = ii-1;
    for(ii=0; ii<=new_length_s/2; ++ii)
    {
        char b_ch = out[ii];
        out[ii] = out[new_length_s-ii];
        out[new_length_s-ii] = b_ch;
    }
    *output = out;
    return 1;
}

int error()
{
    printf("[error]");
    return 0;
}

int main(int argc, char *argv[])
{    
    int p, q;
    char* s=NULL;
    char* out=NULL;
    //Ввод чисел
    if (scanf("%d %d", &p, &q) != 2)
    {
       error();
       return 0;
    }
    
    //Ввод строки
    s = enter();
    if (s==NULL)
    {
        error();
        return 0;
    }
    if (s[0]=='\0' || s[1]=='\0' || s[0]=='\n' || s[1]=='\n')
    {
        error();
        free(s);
        return 0;
    }

    //Перевод и вывод
    if (num_translation(p, q, s+1, &out))
    {
        printf("%s", out);
    }else
    {
        error();
        free(s);
        return 0;
    }

    //Очистка
    if(s!=NULL)
        free(s);
    if(out!=NULL)
        free(out);
    return 0;
}


